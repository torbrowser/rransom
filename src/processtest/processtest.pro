###
### Test invoking Firefox from Qt
### Steven J. Murdoch <http://www.cl.cam.ac.uk/users/sjm217/>
### $Id$
###

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += browserprocess.h processtest.h
SOURCES += browserprocess.cpp main.cpp processtest.cpp
